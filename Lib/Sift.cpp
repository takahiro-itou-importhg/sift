//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**          --  SIFT  (Scale Invariant Feature Transform).  --          **
**                                                                      **
**          Copyright (C), 2016, Takahiro Itou                          **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      An Implementation of MySift class.
**
**      @file       Sift.cpp
**/

#include    "Sift/Sift.h"

#include    <cmath>
#include    <iostream>

#include    <opencv2/highgui/highgui.hpp>
#include    <opencv2/imgproc/imgproc.hpp>

namespace  Sift  {

namespace  {

/**   ガウシアンの分散の初期値。    **/
const   double  SIG0    =  1.6;

/**   分割数。  **/
const   int     S       =  3;

/**   最小サイズ。  **/
const   int     MINW    =  20;

const   double  M_PI    =  3.14159265358979323;

const   double  Rth     =  10.0;
const   double  TH_EDG  =  (Rth + 1) * (Rth + 1) / Rth;
//const   double  TH_POW  =  0.065;
const   double  TH_POW  =  200.0;

}   //  End of (Unnamed) namespace

//========================================================================
//
//    MySift  class.
//

//========================================================================
//
//    Constructor(s) and Destructor.
//

//----------------------------------------------------------------
//    インスタンスを初期化する
//  （デフォルトコンストラクタ）。
//

MySift::MySift()
{
}

//----------------------------------------------------------------
//    インスタンスを破棄する
//  （デストラクタ）。
//

MySift::~MySift()
{
}

//========================================================================
//
//    Public Member Functions (Implement Pure Virtual).
//

//========================================================================
//
//    Public Member Functions (Overrides).
//

//========================================================================
//
//    Public Member Functions (Pure Virtual Functions).
//

//========================================================================
//
//    Public Member Functions (Virtual Functions).
//

//========================================================================
//
//    Public Member Functions.
//

//----------------------------------------------------------------
//    ガウシアンフィルタを適用する。
//

void
MySift::applyGaussianFilter(
        const  Graphic  &imgSrc,
        const  double   sig,
        Graphic         &imgDest)
{
    const  int  W   = imgSrc.size().width;
    const  int  H   = imgSrc.size().height;

    const  int  wWidth  = static_cast<int>(std::ceil(3.0 * sig) * 2 + 1);
    const  int  wRad    = (wWidth - 1) / 2;

    std::vector<double>     msk(wWidth);
    const  double   sigma2  = 2 * sig * sig;
    const  double   div     = std::sqrt(sigma2 * M_PI);

    for ( int x = 0; x < wWidth; ++ x ) {
        const  int  pd  = (x - wRad) * (x - wRad);
        msk[x]  = std::exp(-pd / sigma2) / div;
    }

    //  垂直方向。  //
    Graphic     imgTmp;
    // imgTmp  = cv::Mat(H, W,  CV_8UC1);
    // imgDest = cv::Mat(H, W,  CV_8UC1);
    // imgSrc.copyTo(imgTmp);
    // imgSrc.copyTo(imgDest);
    imgTmp  =  cv::Mat_<TPixel>(H,  W);
    imgDest =  cv::Mat_<TPixel>(H,  W);

    for ( int y = 0; y < H; ++ y ) {
        for ( int x = 0; x < W; ++ x ) {
            double  sum = 0.0;
            for ( int i = 0; i < wWidth; ++ i ) {
                const  int  pos = y + i - wRad;
                if ( (pos < 0) || (H <= pos) ) { continue; }
                sum += msk[i] * getPixel(imgSrc,  x, pos);
            }
            setPixel(imgTmp, x, y)  = sum;
        }
    }

    //  水平方向。  //
    for ( int y = 0; y < H; ++ y ) {
        for ( int x = 0; x < W; ++ x ) {
            double  sum = 0.0;
            for ( int i = 0; i < wWidth; ++ i ) {
                const  int  pos = x + i - wRad;
                if ( (pos < 0) || (W <= pos) ) { continue; }
                sum += msk[i] * getPixel(imgTmp,  pos, y);
            }
            setPixel(imgDest, x, y) = sum;
       }
    }

    return;
}

//----------------------------------------------------------------
//    特徴量を計算する。
//

void
MySift::calcDescriptor(
        const   GraphicsTable   &tblL,
        const   KeyPointsList   &kpList,
        const   GraphicsTable   &tblPow,
        const   GraphicsTable   &tblArg,
        DescriptorList          &dscList)
{
    typedef     KeyPointsList::const_iterator   KeyIter;

    std::cerr   <<  "# DEBUG : START calcDescriptor"    <<  std::endl;

    const  KeyIter  itrEnd  =  kpList.end();
    for ( KeyIter itr = kpList.begin(); itr != itrEnd; ++ itr ) {
        const  KeyPoint  *  const  ptr  = (* itr);
        const  int  o   =  (ptr->oct);
        const  int  s   =  (ptr->scale) - 1;
        const  int  x   =  static_cast<int>(ptr->x);
        const  int  y   =  static_cast<int>(ptr->y);

        double   maxHst  =  * std::max_element(
                ptr->hst,  ptr->hst + KEYPOINT_BIN);
        maxHst  *=  0.8;
        // std::cerr   <<  "# DEBUG : ("   <<  x
        //             <<  ", "            <<  y
        //             <<  ") O = "        <<  o
        //             <<  ", S = "        <<  s
        //             <<  ", MAXH = "     <<  maxHst
        //             <<  std::endl;

        //  キーポイントへの割り当て判定。  //
        for ( int bin = 0; bin < KEYPOINT_BIN; ++ bin ) {
            if ( (ptr->hst[bin]) < maxHst ) {
                continue;
            }

            int bm  =  bin - 1;
            int bp  =  bin + 1;
            if ( bm < 0 ) { bm = KEYPOINT_BIN - 1; }
            if ( bm >= KEYPOINT_BIN ) { bp = 0; }

            if ( (ptr->hst[bin] <= ptr->hst[bm])
                    || (ptr->hst[bin] <= ptr->hst[bp]) )
            {
                continue;
            }

            const   double  sig  =  std::pow(
                    2, (s + 1) / static_cast<double>(S) ) * SIG0;
            const   double  arg =
                (bin / static_cast<double>(KEYPOINT_BIN) );

            Descriptor  *  tmp  =  new  Descriptor(x, y, sig, arg);
            if ( makeDescriptor(tblPow[o][s],  tblArg[o][s],  tmp) <= 0.0 )
            {
                delete  tmp;
                continue;
            }

            //  スケールに合わせて座標を補正（表示用）。    //
            const   double  dlt =  std::pow(2,  o);

            tmp->x      =  x * dlt;
            tmp->y      =  y * dlt;
            tmp->sig    =  sig * dlt;
            tmp->arg    =  (arg - 0.5) * 2 * M_PI;
            dscList.push_back(tmp);
        }
    }

    std::cerr   <<  "# DEBUG : calcDescriptor COMPLETE."
                <<  std::endl;
    return;
}

//----------------------------------------------------------------
//    オリエンテーションを計算する。
//

void
MySift::calcOrientation(
        const   GraphicsTable   &tblL,
        KeyPointsList           &kpList,
        GraphicsTable           &tblPow,
        GraphicsTable           &tblArg)
{
    typedef     KeyPointsList::iterator     KeyIter;

    const  size_t   numOcts = (this->m_numOcts);

    std::cerr   <<  "# DEBUG : START calcOrientation"   <<std::endl;

    //  各平滑化画像の勾配を計算する。  //
    for ( int o = 0; o < numOcts; ++ o ) {
        for ( int s = 0; s < S; ++ s ) {
            const  Graphic  & imgL  =  tblL[o][s+1];

            const  int  W   =  imgL.size().width;
            const  int  H   =  imgL.size().height;

            tblPow[o][s]    =  cv::Mat_<double>(H,  W);
            tblArg[o][s]    =  cv::Mat_<double>(H,  W);

            for ( int y = 0; y < H; ++ y ) {
                for ( int x = 0; x < W; ++ x ) {
                    setPixelT<double>(tblPow[o][s],  x, y)  =  0.0;
                    setPixelT<double>(tblArg[o][s],  x, y)  =  0.0;
                }
            }

            for ( int x = 1; x < W - 1; ++ x ) {
                for ( int y = 1; y < H - 1; ++ y ) {
                    const   double  fu
                        =  getPixel(imgL,  x+1, y)
                        -  getPixel(imgL,  x-1, y);
                    const   double  fv
                        =  getPixel(imgL,  x, y+1)
                        -  getPixel(imgL,  x, y-1);

                    setPixelT<double>(tblPow[o][s],  x, y)
                                =  sqrt(fu * fu + fv * fv);
                    setPixelT<double>(tblArg[o][s],  x, y)
                                =  (atan2(fv, fu) / M_PI + 1) / 2;
                }
            }

        }
    }

    std::cerr   <<  "# DEBUG : Compute POW/ARG Table COMPLETE"
                <<  std::endl;

    //  勾配から各キーポイントのヒストグラムを作る。    //
    std::vector<Graphic>    imgGF(S);
    const   double  dK  =  std::pow(2,  1 / static_cast<double>(S));
    for ( int s = 0; s < S; ++ s ) {
        const  double  sig  =  std::pow(dK, s + 1) * SIG0;
        makeGaussianFilter(sig,  imgGF[s]);
    }

    std::cerr   <<  "# DEBUG : Computing Orientation..."
                <<  std::endl;

    const  KeyIter  itrEnd  =  kpList.end();
    for ( KeyIter itr = kpList.begin();  itr != itrEnd; ++ itr )
    {
        KeyPoint  *  const  ptr = (* itr);
        const  int  o   =  (ptr->oct);
        const  int  s   =  (ptr->scale) - 1;
        const  int  x   =  static_cast<int>(ptr->x);
        const  int  y   =  static_cast<int>(ptr->y);

        if ( (s < 0) || (s >= S) ) {
//            abort();
            continue;
        }

        for ( int bin = 0; bin < KEYPOINT_BIN; ++ bin ) {
            ptr->hst[bin]   =  0;
        }

        const  int  W   =  (tblArg[o][s].size().width);
        const  int  H   =  (tblArg[o][s].size().height);
        // std::cerr   <<  "# DEBUG : Size :"  <<  W
        //             <<  ", "                <<  H   <<  std::endl;

        const  int  Rm  =  (imgGF[s].size().width - 1) / 2;
        for ( int i = x - Rm; i <= x + Rm; ++ i ) {
            if ( (i < 0) || (W <= i) ) {
                // std::cerr   <<  "# DEBUG : Out Of Bounds (X)."  <<  i
                //             <<  std::endl;
                continue;
            }
            const  int  mx  =  i - x + Rm;

            for ( int j = y - Rm; j <= y + Rm; ++ j ) {
                if ( (j < 0) || (H <= j) ) {
                    // std::cerr   <<  "# DEBUG : Out Of Bounds (Y)."  <<  j
                    //         <<  std::endl;
                    continue;
                }
                const  int  my  =  j - y + Rm;

                const  int  bin =  static_cast<int>(
                        std::floor(
                                KEYPOINT_BIN
                                *  getPixelT<double>(tblArg[o][s], i, j)
                        )
                ) % KEYPOINT_BIN;
                if ( (bin < 0) || (bin >= KEYPOINT_BIN) ) {
                    std::cerr   <<  "# FATAL : Index Out Of Bounds. bin = "
                            <<  bin     <<  std::endl;
                    abort();
                }
                ptr->hst[bin]   +=
                        getPixelT<double>(imgGF[s],  mx, my)
                        *  getPixelT<double>(tblPow[o][s],  i, j);
            }
        }

        //  ヒストグラムのスムージング。    //
        for ( int i = 0; i < 6; ++ i ) {
            ptr->hst[0] = ( (ptr->hst[KEYPOINT_BIN - 1])
                           + (ptr->hst[0]) + (ptr->hst[1]) ) / 3.0;
            for ( int bin = 1; bin < KEYPOINT_BIN; ++ bin ) {
                ptr->hst[bin]   =  ( (ptr->hst[(bin+1) % KEYPOINT_BIN])
                                     + (ptr->hst[bin-1]) + (ptr->hst[bin]) )
                        / 3.0;
            }
        }
    }

    std::cerr   <<  "# DEBUG : calcOrientation COMPLETE."
                <<  std::endl;

    return;
}

//----------------------------------------------------------------
//    特徴量記述子の内容を描画する。
//

template  <typename  TKIter>
void
MySift::drawDescriptors(
        const   TKIter  itrHead,
        const   TKIter  itrTail,
        Graphic         &imgTrg)
{
    for ( TKIter itr = itrHead; itr != itrTail; ++ itr )
    {
        const  Descriptor  *   ptr  =  (* itr);
//        const  double   dlt = std::pow(2, (ptr->oct));

        const  double   cr  =  (ptr->sig);
        const  double   xo  =  (ptr->x);
        const  double   yo  =  (ptr->y);
        const  double   arg =  (ptr->arg);

        cv::circle(imgTrg,  cv::Point(xo, yo),  static_cast<int>(cr),
                   cv::Scalar(0, 0, 200), 2, 4);

        cv::line(imgTrg,  cv::Point(xo, yo),
                 cv::Point(xo + cr * cos(arg), yo + cr * sin(arg)),
                 cv::Scalar(0, 200, 0),  2, 4);

#if defined( _DEBUG )
        std::cout   <<  "Descriptor = ("
                    <<  xo  <<  ", "
                    <<  yo  << ") R ="  <<  cr
                    <<  " ARG = "       <<  arg
                    <<  std::endl;
#endif
    }

    return;

}

//----------------------------------------------------------------
//    キーポイントを描画する。
//

template  <typename  TKIter>
void
MySift::drawKeyPoints(
        const   TKIter  itrHead,
        const   TKIter  itrTail,
        Graphic         &imgTrg)
{
    for ( TKIter itr = itrHead; itr != itrTail; ++ itr )
    {
        const  KeyPoint  *  ptr = (* itr);
        const  double   dlt = std::pow(2, (ptr->oct));

        const  double   cr  = dlt * std::pow(
                2, ((ptr->scale) - 1) / static_cast<double>(S));
        const  double   xo  = (ptr->x) * dlt;
        const  double   yo  = (ptr->y) * dlt;

        cv::circle(imgTrg,  cv::Point(xo, yo),  static_cast<int>(cr),
                   cv::Scalar(0, 0, 200), 3, 4);

#if defined( _DEBUG )
        std::cout   <<  "Key Point = ("
                    <<  xo  <<  ", "
                    <<  yo  << ") R ="  <<  cr
                    <<  std::endl;
#endif
    }

    return;
}

//----------------------------------------------------------------
//    アルゴリズムを実行する。
//

void
MySift::executeSIFT(
        const  Graphic  &imgSrc,
        Graphic         &imgOut)
{
    //  画像をグレースケール変換。  //
    Graphic     imgGray;
    cv::cvtColor(imgSrc,  imgGray,  CV_RGB2GRAY);

    //  キーポイント候補リスト。    //
//    KeyPointsList   keysList;
    DescriptorList  dscList;

    //  アルゴリズムの本体を実行。  //
//    executeSIFT(imgGray,  keysList,  dscList);
    executeSIFT(imgGray,  dscList);

    //  出力画像を生成する。        //
    imgSrc.copyTo(imgOut);
//    drawKeyPoints(keysList.begin(),  keysList.end(),  imgOut);
    drawDescriptors(dscList.begin(),  dscList.end(),  imgOut);

    return;
}

//----------------------------------------------------------------
//    アルゴリズムを実行する。
//

void
MySift::executeSIFT(
        const  Graphic  &imgSrc,
        Graphic         &imgGray,
        Graphic         &imgOut)
{
    //  画像をグレースケール変換。  //
    cv::cvtColor(imgSrc,  imgGray,  CV_RGB2GRAY);

    //  キーポイント候補リスト。    //
//    KeyPointsList   keysList;
    DescriptorList  dscList;

    //  アルゴリズムの本体を実行。  //
//    executeSIFT(imgGray,  keysList,  dscList);
    executeSIFT(imgGray,  dscList);

    //  出力画像を生成する。        //
//    imgGray.copyTo(imgOut);
    cv::cvtColor(imgGray,  imgOut,  CV_GRAY2RGB);
//    drawKeyPoints(keysList.begin(),  keysList.end(),  imgOut);
    drawDescriptors(dscList.begin(),  dscList.end(),  imgOut);

    return;
}

//----------------------------------------------------------------
//    アルゴリズムを実行する。
//

void
MySift::executeSIFT(
        const  Graphic  &imgSrc,
        DescriptorList  &dscList)
{
    std::cerr   <<  "# DEBUG : imgSrc.  W = "   <<  imgSrc.size().width
                <<  ", H = "        <<  imgSrc.size().height
                <<  ", depth = "    <<  imgSrc.elemSize1()
                <<  ", channel = "  <<  imgSrc.channels()
                <<  std::endl;

    KeyPointsList   keysList;

    //  オクターブ数を決定。    //
    size_t  oct = 0;
    int     nW  = std::min(imgSrc.size().width,  imgSrc.size().height);
    for ( oct = 0; nW > MINW; ++ oct, nW /= 2 ) { }

    this->m_numOcts = oct;
    std::cerr   <<  "Number Of Octaves = "  <<  oct <<  std::endl;

    //  バッファを確保する。    //
    GraphicsTable   tblL  (oct);
    GraphicsTable   tblDoG(oct);
    GraphicsTable   tblPow(oct);
    GraphicsTable   tblArg(oct);
    for ( size_t i = 0; i < oct; ++ i ) {
        tblL  [i].resize(S + 3);
        tblDoG[i].resize(S + 2);
        tblPow[i].resize(S);
        tblArg[i].resize(S);
    }

    //  Detection.  //
    getKeyPoints(imgSrc,  tblL,  tblDoG,  keysList);
    std::cerr   <<  "# INFO : The Number of KeyPoints = "
                <<  keysList.size()     <<  std::endl;
    localizeKeyPoints(imgSrc,  tblDoG,  keysList);

    //  Description.    //
    calcOrientation(tblL,  keysList,  tblPow,  tblArg);
    calcDescriptor (tblL,  keysList,  tblPow,  tblArg,  dscList);

    std::cerr   <<  "# INFO : The Number of Descriptors = "
                <<  dscList.size()      <<  std::endl;

    return;
}

//----------------------------------------------------------------
//    スケールとキーポイント検出。
//

void
MySift::getKeyPoints(
        const  Graphic  &imgSrc,
        GraphicsTable   &tblL,
        GraphicsTable   &tblDoG,
        KeyPointsList   &keys)
{
    //  増加率。    //
    const   double  k   =  std::pow(2, 1 / static_cast<double>(S));

    Graphic     imgCopy1,  imgCopy2;

    imgSrc.copyTo(imgCopy1);

    //  平滑化画像を作成。  //
    const  size_t   numOcts = (this->m_numOcts);
    for ( int oct = 0; oct < numOcts; ++ oct ) {
        double  sig = SIG0;
        for ( int s = 0; s < S + 3; ++ s ) {
            //  元画像にガウシアンフィルタ。    //
            applyGaussianFilter(imgCopy1,  sig,  tblL[oct][s]);
            std::cerr   <<  "# Octave = "   <<  oct
                        <<  ", Sigma["  <<  s
                        <<  "] = "      <<  sig
                        <<  ", Size = " << tblL[oct][s].size()
                        <<  std::endl;
            sig *= k;
        }

        //  ダウンサンプリング。    //
        downSampling(imgCopy1,  imgCopy2);
        imgCopy2.copyTo(imgCopy1);
        std::cerr   <<  "# Down Sampling, New Size = "
                    <<  imgCopy1.size() <<  std::endl;
    }

    //  DoG 画像を作成。    //
    for ( int oct = 0; oct < numOcts; ++ oct ) {
        for ( int i = 0; i < S + 2; ++ i ) {
            diffGraphics(
                    tblL  [oct][i + 1],
                    tblL  [oct][i],
                    tblDoG[oct][i]);
        }
    }

    //  キーポイントが既出かどうかのフラグ。    //
    std::vector< std::vector<bool> >    flg;
    {
        const  int  W   = tblDoG[0][0].size().width;
        const  int  H   = tblDoG[0][0].size().height;
        flg.resize(W);
        for ( int x = 0; x < W; ++ x ) {
            flg[x].resize(H);
        }
    }

    //  DoG 画像から極値検出。  //
    for ( int oct = 0; oct < numOcts; ++ oct ) {
        const  size_t   W   = tblDoG[oct][0].size().width;
        const  size_t   H   = tblDoG[oct][0].size().height;

        //  フラグ初期化。  //
        for ( int y = 1; y < H - 2; ++ y ) {
            for ( int x = 1; x < W - 1; ++ x ) {
                flg[x][y]   = false;
            }
        }

        for ( int s = 1; s < S+1 ; ++ s ) {
            for ( int y = 2; y < H - 2; ++ y ) {
                for ( int x = 2; x < W - 2; ++ x ) {
                    //  既出のピクセルはキーポイントにしない。  //
                    if ( flg[x][y] ) { continue; }

        //  近傍 (26) を探索。    //
        bool    isMax   = true;
        bool    isMin   = true;
        KeyPoint  * tmp = NULL;
        for ( int ds = s - 1; ds <= s + 1; ++ ds ) {
            for ( int dy = y - 1; dy <= y + 1; ++ dy ) {
                for ( int dx = x - 1; dx <= x + 1; ++ dx ) {
                    //  中心は無視。    //
                    if ( ds == s && dx ==x && dy == y ) { continue; }

                    //  極大、極小判定。    //
                    if ( isMax &&
                            (getPixel(tblDoG[oct][s],  x, y)
                             <= getPixel(tblDoG[oct][ds],  dx, dy) )
                    )
                    {
                        isMax   = false;
                    }

                    if ( isMin &&
                            (getPixel(tblDoG[oct][s],  x, y)
                             >= getPixel(tblDoG[oct][ds],  dx, dy) )
                    )
                    {
                        isMin   = false;
                    }

                    if ( !isMax && !isMin ) { goto LabelNextKeyPoint; }
                }   //  Next (dx)
            }   //  Next (dy)
        }   //  Next (ds)

        //  キーポイント候補として登録。    //
        tmp = new  KeyPoint((double)x, (double)y, oct, s);
        keys.push_back(tmp);
        flg[x][y]   = true;

LabelNextKeyPoint:
        ;
                }   //  Next (x)
            }   //  Next (y)
        }   //  Next (s)
    }   //  Next (oct)

    std::cerr   <<  "# The Number of KeyPoints = "  <<  keys.size()
                <<  std::endl;
    return;
}

//----------------------------------------------------------------
//    キーポイントのローカライズ。
//

void
MySift::localizeKeyPoints(
        const  Graphic          &imgSrc,
        const  GraphicsTable    &tblDoG,
        KeyPointsList           &kpList)
{
    std::cout   <<  "Step2 : Localize." <<  std::endl;

    typedef     KeyPointsList::const_iterator   KeyIter;

    double  mD[3][3];
    double  iD[3][3];
    double  xD[3];
    double  spX[3];

    const  KeyIter  itrEnd  =  kpList.end();
    for ( KeyIter itr = kpList.begin(); itr != itrEnd; )
    {
        const  KeyPoint  *  const   ptrKey  =  (* itr);

        //  2.2.1.  主曲率による絞込み
        int  o  =  (ptrKey->oct);
        int  s  =  (ptrKey->scale);
        int  x  =  (ptrKey->x);
        int  y  =  (ptrKey->y);

        //  ヘッセ行列を求める。    //
        double  dxx =  (getPixel(tblDoG[o][s],  x-2, y)
                        +      getPixel(tblDoG[o][s],  x+2, y)
                        -  2 * getPixel(tblDoG[o][s],  x, y) );
        double  dyy =  (getPixel(tblDoG[o][s],  x, y-2)
                        +      getPixel(tblDoG[o][s],  x, y+2)
                        -  2 * getPixel(tblDoG[o][s],  x, y) );
        double  dxy =  (getPixel(tblDoG[o][s],  x-1, y-1)
                        -  getPixel(tblDoG[o][s],  x-1, y+1)
                        -  getPixel(tblDoG[o][s],  x+1, y-1)
                        +  getPixel(tblDoG[o][s],  x+1, y+1) );
        const  double  trc  =  dxx + dyy;
        const  double  det  =  dxx * dyy - dxy * dxy;

        if ( trc * trc / det >= TH_EDG ) {
            delete  ptrKey;
            itr =  kpList.erase(itr);
            continue;
        }

#if 1
        //----------------------------------------

        //  コントラストが低いキーポイントを削除。  //
        const  int  sm1 =  (s-1 < 0) ? 0 : s-1;
        const  int  sm2 =  (s-2 < 0) ? 0 : s-2;
        const  int  sp1 =  (s+1 >= S+2) ? S+1 : s+1;
        const  int  sp2 =  (s+2 >= S+2) ? S+1 : s+2;

        double  dx  =  (getPixel(tblDoG[o][s],  x-1, y)
                        -  getPixel(tblDoG[o][s],  x+1, y) );
        double  dy  =  (getPixel(tblDoG[o][s],  x, y-1)
                        -  getPixel(tblDoG[o][s],  x, y+1) );
        double  ds  =  (getPixel(tblDoG[o][sm1],  x, y)
                        -  getPixel(tblDoG[o][sp1],  x, y) );

        double  dss =  (getPixel(tblDoG[o][sm2],  x, y)
                        +     getPixel(tblDoG[o][sp2],  x, y)
                        - 2 * getPixel(tblDoG[o][s],  x, y) );
        double  dxs =  (getPixel(tblDoG[o][sm1],  x-1, y)
                        - getPixel(tblDoG[o][sm1],  x+1, y)
                        + getPixel(tblDoG[o][sp1],  x-1, y)
                        - getPixel(tblDoG[o][sp1],  x+1, y) );
        double  dys =  (getPixel(tblDoG[o][sm1],  x, y-1)
                        -  getPixel(tblDoG[o][sm1],  x, y+1)
                        +  getPixel(tblDoG[o][sp1],  x, y-1)
                        -  getPixel(tblDoG[o][sp1],  x, y+1) );

        mD[0][0]  =  dxx;   mD[0][1]  =  dxy;   mD[0][2]  =  dxs;
        mD[1][0]  =  dxy;   mD[1][1]  =  dyy;   mD[1][2]  =  dys;
        mD[2][0]  =  dxs;   mD[2][1]  =  dys;   mD[2][2]  =  dss;
        xD[0]   =  - dx;    xD[1]   =  - dy;    xD[2]   =  -  ds;

        calcInvMatrix<3>(mD, iD);
        spX[0]  =  iD[0][0] * xD[0] + iD[0][1] * xD[1] + iD[0][2] * xD[2];
        spX[1]  =  iD[1][0] * xD[0] + iD[1][1] * xD[1] + iD[1][2] * xD[2];
        spX[2]  =  iD[2][0] * xD[0] + iD[2][1] * xD[1] + iD[2][2] * xD[2];

        double  dblPow
                =  fabs(getPixel(tblDoG[o][s],  x, y)
                        + (xD[0]*spX[0] + xD[1]*spX[1] + xD[2]*spX[2]) / 2
                );
        if ( dblPow < TH_POW ) {
            delete  ptrKey;
            itr =  kpList.erase(itr);
            continue;
        }
#endif

        ++  itr;
    }

    std::cerr   <<  "# INFO : Reduced KeyPoints : "
                <<  kpList.size()   <<  std::endl;
    return;
}

//========================================================================
//
//    Protected Member Functions.
//

//========================================================================
//
//    For Internal Use Only.
//

//----------------------------------------------------------------
//    画像の差分。
//

inline  void
MySift::diffGraphics(
        const  Graphic  &imgLhs,
        const  Graphic  &imgRhs,
        Graphic         &imgDest)
{
    const  size_t   W
        = std::min(imgLhs.size().width,  imgRhs.size().width);
    const  size_t   H
        = std::min(imgLhs.size().height, imgRhs.size().height);

    imgDest = cv::Mat_<TPixel>(H, W);

    for ( size_t y = 0; y < H; ++ y ) {
        for ( size_t x = 0; x < W; ++ x ) {
            setPixel(imgDest,  x, y)
                    =  getPixel(imgLhs,  x, y)
                    -  getPixel(imgRhs,  x, y);
        }
    }

    return;
}

//----------------------------------------------------------------
//    ダウンサンプリング。
//

inline  void
MySift::downSampling(
        const  Graphic  &imgSrc,
        Graphic         &imgDest)
{
    const  size_t   W   = imgSrc.size().width  / 2;
    const  size_t   H   = imgSrc.size().height / 2;

    imgDest = cv::Mat_<TPixel>(H,  W);

    for ( int y = 0; y < H; ++ y ) {
        for ( int x = 0; x < W; ++ x ) {
            setPixel(imgDest,  x, y)
                    =  getPixel(imgSrc,  x * 2, y * 2);
        }
    }

    return;
}

//----------------------------------------------------------------
//    逆行列の計算。
//

template  <int  N>
inline  void
MySift::calcInvMatrix(
        double  dMat[N][N],
        double  dInv[N][N])
{
    double  dblBuf;

    for ( int i = 0; i < N; ++ i ) {
        for ( int j = 0; j < N; ++ j ) {
            dInv[i][j]  =  0;
        }
        dInv[i][i]  =  1;
    }

    for ( int i = 0; i < N; ++ i ) {
        dblBuf  =  1 / dMat[i][i];
        for ( int j = 0; j < N; ++ j ) {
            dMat[i][j]  *=  dblBuf;
            dInv[i][j]  *=  dblBuf;
        }
        for ( int j = 0; j < N; ++ j ) {
            if ( i == j ) {
                continue;
            }
            dblBuf  =  dMat[j][i];
            for ( int k = 0; k < N; ++ k ) {
                dMat[j][k]  -=  dMat[i][k] * dblBuf;
                dInv[j][k]  -=  dInv[i][k] * dblBuf;
            }
        }
    }

    return;
}

//----------------------------------------------------------------
//    画像からピクセル値の参照を取り出す。
//

template  <typename  T>
inline  const   T  &
MySift::getPixelT(
        const  Graphic  &imgSrc,
        const  int      sx,
        const  int      sy)
{
    const  int  xW  =  imgSrc.size().width;
    const  int  yH  =  imgSrc.size().height;

    if ( (sx < 0) || (xW <= sx) || (sy < 0) || (yH <= sy) ) {
        std::cerr   <<  "# FATAL : getPixel : Out Of Bounds : ("
                    <<  sx  <<  ", "    <<  sy
                    <<  ") W = "    <<  xW
                    <<  ", H = "    <<  yH  <<  std::endl;
        exit(1);
    }

    return ( imgSrc.at<T>(sy, sx) );
}

//----------------------------------------------------------------
//    特徴量を生成する。
//

inline  double
MySift::makeDescriptor(
        const  Graphic  &tblPow,
        const  Graphic  &tblArg,
        Descriptor  *   ptrDsc)
{
    const  int      x0  =  static_cast<int>(ptrDsc->x);
    const  int      y0  =  static_cast<int>(ptrDsc->y);
    const  double   s0  =  (ptrDsc->sig);
    const  double   a0  =  (ptrDsc->arg);

    const  double   cos0    =  std::cos( (a0 - 0.5) * 2.0 * M_PI );
    const  double   sin0    =  std::sin( (a0 - 0.5) * 2.0 * M_PI );

    const  int  mW  =  static_cast<int>(std::ceil(3.0 * s0) * 2 + 1);
    const  int  mR  =  (mW - 1) / 2;
    const  int  gW  =  tblPow.size().width;
    const  int  gH  =  tblPow.size().height;

    for ( int bin = 0; bin < DESCRIPTOR_BIN_SIZE; ++ bin ) {
        ptrDsc->v[bin]  =  0;
    }

    for ( int j = y0 - mR; j <= y0 + mR; ++ j ) {
        const  int  mY  =  j - y0 + mR;
        const  int  bY  =  4 * mY / mW;

        for ( int i = x0 - mR; i <= x0 + mR; ++ i ) {
            const  int  mX  =  i - x0 + mR;
            const  int  bX  =  4 * mX / mW;

            //  座標を回転させる。  //
            const  int  pX  =  x0
                + static_cast<int>( cos0 * (i - x0) - sin0 * (j - y0) );
            const  int  pY  =  y0
                + static_cast<int>( sin0 * (i - x0) + cos0 * (j - y0) );

            if ( (pX < 0) || (gW <= pX) || (pY < 0) || (gH <= pY) ) {
                continue;
            }

            //  勾配方向を回転させる。  //
            double  arg =  getPixelT<double>(tblArg, pX, pY) - a0;
            if ( arg < 0 ) {
                arg += 1;
            } else if ( arg >= 1 ) {
                arg -= 1;
            }

            //  ビンの番号。    //
            const  int  bin =  static_cast<int>(std::floor(8 * arg))
                    + (bX * 8) + (bY * 4 * 8);

            //  ヒストグラム。  //
#if defined( _DEBUG )
            std::cerr   <<  "# DEBUG : pX = "   <<  pX
                        <<  ", pY = "           <<  pY
                        <<  ", VAL = "
                        <<  getPixelT<double>(tblPow,  pX, pY)
                        <<  ", BIN = "          <<  bin
                        <<  ", ARG = "          <<  arg
                        <<  std::endl;
#endif
            ptrDsc->v[bin]  +=  getPixelT<double>(tblPow,  pX, pY);
        }
    }

    //  ヒストグラムを正規化。  //
    double  dblSum  =  0.0;
    for ( int bin = 0; bin < DESCRIPTOR_BIN_SIZE; ++ bin ) {
        dblSum  +=  (ptrDsc->v[bin] * ptrDsc->v[bin]);
    }
    dblSum  =  std::sqrt(dblSum);
    if ( dblSum == 0.0 ) {
        std::cerr   <<  "# DEBUG : Zero Vec. @ "
                    <<  ", mW="     <<  mW
                    <<  ", mR="     <<  mR
                    <<  ", X="      <<  x0
                    <<  ", Y="      <<  y0
                    <<  std::endl;
        return ( dblSum );
    }

    for ( int bin = 0; bin < DESCRIPTOR_BIN_SIZE; ++ bin ) {
        ptrDsc->v[bin]  /=  dblSum;
    }

    return ( dblSum );
}

//----------------------------------------------------------------
//    ガウシアンフィルタを作成する。
//

inline  void
MySift::makeGaussianFilter(
        const   double  sig,
        Graphic         &imgMsk)
{
    const  int  wWidth  = static_cast<int>(std::ceil(3.0 * sig) * 2 + 1);
    const  int  wRad    = (wWidth - 1) / 2;

    imgMsk  =  cv::Mat(wWidth,  wWidth,  CV_64FC1);
    const  double   sigma2  =  2 * sig * sig;
    const  double   div     =  (sigma2 * M_PI);

    for ( int x = 0; x < wWidth; ++ x ) {
        const  int  px  =  x - wRad;
        for ( int y = 0; y < wWidth; ++ y ) {
            const   int     py  =  y - wRad;
            const   double  pd  =  (px * px) * (py * py);
            setPixelT<double>(imgMsk,  x, y)
                        =  std::exp(- pd / sigma2) / div;
        }
    }

    return;
}

//----------------------------------------------------------------
//    画像からピクセル値の参照を取り出す。
//

template  <typename  T>
inline  T  &
MySift::setPixelT(
        Graphic     &imgTrg,
        const  int  dx,
        const  int  dy)
{
    const  int  xW  =  imgTrg.size().width;
    const  int  yH  =  imgTrg.size().height;

    if ( (dx < 0) || (xW <= dx) || (dy < 0) || (yH <= dy) ) {
        std::cerr   <<  "# FATAL : setPixel : Out Of Bounds : ("
                    <<  dx  <<  ", "    <<  dy
                    <<  ") W = "    <<  xW
                    <<  ", H = "    <<  yH  <<  std::endl;
        exit(1);
    }

    return ( imgTrg.at<T>(dy, dx) );
}

}   //  End of namespace  Sift
