//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**          --  SIFT  (Scale Invariant Feature Transform).  --          **
**                                                                      **
**          Copyright (C), 2016, Takahiro Itou                          **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

#include    "Sift/Sift.h"

#include    <opencv2/highgui/highgui.hpp>
#include    <opencv2/imgproc/imgproc.hpp>

#include    <iostream>

template  <int  N>
inline  double
computeCosSimilar(
        const  double  (& vA)[N],
        const  double  (& vB)[N] )
{
    double  dblNum  =  0.0;
    double  dblNA2  =  0.0;
    double  dblNB2  =  0.0;

    for ( int i = 0; i < N; ++ i ) {
        dblNum  +=  vA[i] * vB[i];
        dblNA2  +=  vA[i] * vA[i];
        dblNB2  +=  vB[i] * vB[i];
    }
    return ( dblNum / std::sqrt(dblNA2 * dblNB2) );
}

int  main(int argc,  char * argv[])
{
    typedef     Sift::MySift::DescriptorList    DescriptorList;
    typedef     DescriptorList::const_iterator  DscIter;
    typedef     Sift::MySift::Descriptor        Descriptor;

    Sift::MySift    sift;
    int             szLine  =  5;
    int             cntGet  =  0;
    int             cntCmp  =  0;

//    cv::namedWindow("Source Image",  cv::WINDOW_AUTOSIZE);
    cv::namedWindow("Output Image",  cv::WINDOW_AUTOSIZE);

    cv::Mat     imgSrc1,  imgSrc2,  imgGry1,  imgGry2;
    cv::Mat     imgOut;
    Sift::MySift::DescriptorList    dscLst1,  dscLst2;


//    imgSrc  = cv::imread("Source.jpg");
//    imgSrc  = cv::imread("ASICS_0001.jpg");
//    imgSrc  = cv::imread("ASICS_0002.jpg");
    if ( argc >= 3 ) {
        imgSrc1 = cv::imread(argv[1]);
        imgSrc2 = cv::imread(argv[2]);
    } else {
        imgSrc1 = cv::imread("ASICS_0001.jpg");
        imgSrc2 = cv::imread("ASICS_0003.jpg");
    }
    if ( argc >= 5 ) {
        szLine  =  atoi(argv[4]);
    }
    if ( argc >= 4 ) {
        cntGet  =  atoi(argv[3]);
    }

    //  画像をグレースケール変換。  //
    cv::cvtColor(imgSrc1,  imgGry1,  CV_RGB2GRAY);
    cv::cvtColor(imgSrc2,  imgGry2,  CV_RGB2GRAY);

//    cv::imshow("Source Image",  imgSrc);

//    sift.executeSIFT(imgSrc,  imgOut);
//    sift.executeSIFT(imgSrc,  imgGry,  imgOut);

    const  int  W1  =  imgSrc1.size().width;
    const  int  H1  =  imgSrc1.size().height;
    const  int  W2  =  imgSrc2.size().width;
    const  int  H2  =  imgSrc2.size().height;

    imgOut  =  cv::Mat_<cv::Vec3b>(H1 + H2, W1 + W2,  3);
    for ( int y = 0; y < H1 + H2; ++ y ) {
        for ( int x = 0; x < W1 + W2; ++ x ) {
            imgOut.at<cv::Vec3b>(y, x)[0]   =  128;
            imgOut.at<cv::Vec3b>(y, x)[1]   =  128;
            imgOut.at<cv::Vec3b>(y, x)[2]   =  128;

        }
    }

    for ( int y = 0; y < H1; ++ y ) {
        for ( int x = 0; x < W1; ++ x ) {
            imgOut.at<cv::Vec3b>(y, x)
                        =  imgSrc1.at<cv::Vec3b>(y, x);
        }
    }
    for ( int y = 0; y < H2; ++ y ) {
        for ( int x = 0; x < W2; ++ x ) {
            imgOut.at<cv::Vec3b>(y + H1, x + W1)
                        =  imgSrc2.at<cv::Vec3b>(y, x);
        }
    }

    sift.executeSIFT(imgGry1,  dscLst1);
    sift.executeSIFT(imgGry2,  dscLst2);

    cv::Scalar      tblClr[7] = {
        cv::Scalar(  0,   0,   0),
        cv::Scalar(  0,   0, 200),
        cv::Scalar(  0, 200,   0),
        cv::Scalar(200,   0,   0),
        cv::Scalar(  0, 200, 200),
        cv::Scalar(200,   0, 200),
        cv::Scalar(200, 200,   0)
    };

    const  DscIter  itEnd1  =  dscLst1.end();
    const  DscIter  itEnd2  =  dscLst2.end();
    int             idxClr  =  0;

    for ( DscIter it1 = dscLst1.begin(); it1 != itEnd1; ++ it1 ) {
        const   Descriptor  *   p1  =  (* it1);
        double  maxCos  =  - 20000.0;
        int     mX      =  0;
        int     mY      =  0;

        for ( DscIter it2 = dscLst2.begin(); it2 != itEnd2; ++ it2 ) {
            const   Descriptor  *   p2  =  (* it2);

            const   double
                theCos  =  computeCosSimilar(p1->v,  p2->v);
            if ( maxCos < theCos ) {
                maxCos  =  theCos;
                mX      =  p2->x;
                mY      =  p2->y;
            }
        }
        if ( maxCos < -1.0 || 1.0 < maxCos ) {
            std::cerr   <<  "# DEBUG : Invalid COS = "  <<  maxCos
                        <<  std::endl;
        }
        if ( maxCos < 0.9 ) {
            //  該当なし。  //
            continue;
        }

        cv::line(imgOut,  cv::Point(p1->x,  p1->y),
                 cv::Point(mX + W1,  mY + H1),
                 tblClr[idxClr % 7],  szLine, 4);
        ++  idxClr;
        if ( (cntGet > 0) && (idxClr >= cntGet) ) {
            break;
        }
    }

    std::cout   <<  "Total : "  <<  idxClr  <<  " Point(s) Match."
                <<  std::endl;

//    cv::imshow("Source Image",  imgGry);
    cv::imshow("Output Image",  imgOut);
    cv::waitKey(0);

    return ( 0 );
}
